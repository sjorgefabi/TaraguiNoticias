<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use \Carbon\Carbon;
class setLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //before
        DB::statement("SET lc_time_names = 'es_ES'");
        // Localization Carbon
        Carbon::setUTF8(true);
        setlocale(LC_TIME, 'spanish');

            return $next($request);
    }
}
