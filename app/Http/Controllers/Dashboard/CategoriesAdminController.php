<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;

use App\Categories;
use Illuminate\Http\Request;
use App\Http\Requests\CreateCategoriesRequest;
class CategoriesAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categs = Categories::orderBy('id', 'desc')->get();
        $cantcat = Categories::count();
        return view('dashboard.section.categories.index')->with(['categs'=>$categs, 'cantcat' => $cantcat]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Categories $data)
    {
       return view('dashboard.section.categories.form')->with(['data'=>$data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoriesRequest $request)
    {
        Categories::create($request->only('descripcion'));
        return redirect()->route('path_categorias')->with('message', 'Nueva categoria creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Categories::findOrFail($id);
        return view('dashboard.section.categories.show')->with(['data'=>$data]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Categories::FindOrFail($id);
        return view('dashboard.section.categories.form')->with(['data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Categories::findOrFail($id)->update($request->only('descripcion'));
            $this->validate($request, [
            'descripcion'=> 'required'
            ]);
        return redirect()->route('path_categorias')->with('message', 'Categoria actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function destroy(Categories $id)
    {
        $id->delete();
        return redirect()->route('path_categorias');
    }*/
}
