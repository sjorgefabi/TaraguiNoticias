<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;

use App\Ads;
use Illuminate\Http\Request;
use File;


class AdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ads = Ads::all();
        return view('dashboard.section.anuncios.index')->with(['ads'=>$ads]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Ads $ads)
    {
        return view('dashboard.section.anuncios.formulario')->with(['data'=>$ads]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $ads = new Ads;
        $ads->nombre = $request->nombre;

        if ($request->hasFile('imagen-ads')) {
            $file = $request->file('imagen-ads'); //or $request->imagen_ads
            $imagen_titulo = time().'.'.$file->getClientOriginalExtension();
            $file->move('imagenes_anuncios/', $imagen_titulo);  
              $ads->imagen = $imagen_titulo;
          }
        $ads->save();

        return redirect()->route('Ads.index')->with('message', 'datos guardados correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ads  $ads
     * @return \Illuminate\Http\Response
     */
    public function show($ads)
    {
        $result = Ads::findOrFail($ads);
        return view('dashboard.section.anuncios.show')->with(['ads'=>$result]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ads  $ads
     * @return \Illuminate\Http\Response
     */
    public function edit($ads)
    {
      $result = Ads::findOrFail($ads);
      return view('dashboard.section.anuncios.formulario')->with(['data'=>$result]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ads  $ads
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ads)
    {
       $result = Ads::findOrFail($ads);
    

        File::delete('imagenes_anuncios/'.$result->imagen);
        if ($request->hasFile('imagen-ads')) {
            $file = $request->file('imagen-ads');
            $nombreImagen = time().'.'.$file->getClientOriginalExtension();
            $file->move('imagenes_anuncios/', $nombreImagen);
            
        }
         
       $result->nombre = $request->input('nombre');
       $result->imagen = $nombreImagen;
       $result->save();

       return redirect()->route('Ads.index')->with('message', 'datos actualizados'); 
   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ads  $ads
     * @return \Illuminate\Http\Response
     */
    public function destroy($ads)
    {
        $result = Ads::findOrFail($ads);
        $result->delete();
        File::delete('imagenes_anuncios/'.$result->imagen);

        return redirect()->route('Ads.index')->with('message', 'datos eliminados');
    }

}
