<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\News;
use App\Categories;
use App\Http\Requests\CreateNewsRequest;
use App\Http\Requests\UpdateNewsRequest;
use DB;
use File;
use Carbon\Carbon;
class NewsAdminController extends Controller
{
   public function index(){
      //news relacionadas con el usuario y categorias
     $news = News::with('categories')->orderBy('id', 'desc')->paginate(5);
     return view('dashboard.section.tables')->with(['news'=>$news]);
   }

   public function show(News $new){
   	return view('dashboard.section.show')->with(['new'=>$new]);
   }

   public function create(News $new){
         $categorias = Categories::all();
return view('dashboard.section.form')->with(['new'=>$new, 'categorias'=>$categorias]);
   }

   public function store(CreateNewsRequest $request){ 
          $new = new News;
          $new->titulo = $request->input('titulo');
          $new->descripcion = $request->descripcion;
          $new->categoria_id = $request->categoria_id;
          $new->publicacion = $request->publicacion;
          $new->destacada = $request->destacada;
          $new->portada = $request->portada;

          if ($request->hasFile('imagen_destacada')) {
            $file = $request->file('imagen_destacada'); //or $request->imagen_destacada
            $imagen_titulo = time().'.'.$file->getClientOriginalExtension();
            $directorio = 'imagenes_destacadas/';
             $file->move($directorio, $imagen_titulo);
            /*if (!File::exists($directorio )) {
               $resultado = File::makeDirectory($directorio , 0777, true);
               $file->move($directorio, $imagen_titulo);
            }*/
              
              $new->imagen_destacada = $imagen_titulo;
          }

          $new->save();
   	  //News::create($request->only('titulo', 'descripcion', 'imagen_destacada','categoria_id'));
   	 return redirect()->route('path_noticias')->with('message', 'nueva noticia creada');
          /*//Si hay mas usuarios asignados..
         $new  = new News;
         $new->fill(
            $request->only('titulo', 'descripcion')
         );
         $new->user_id = $auth->user()->id;*/
   }
   
   public function edit(News $new){
      $categoria = Categories::select('id','descripcion')->get();
   	return view('dashboard.section.form')->with(['new'=>$new, 'categorias'=>$categoria]);
      /*si hay muchos usuarios
      if ($new->user_id =! Auth::user()->id) { return redirect()->route('path_noticias');
      }*/
   }

   public function update(UpdateNewsRequest $request, News $new){
      if ($request->hasFile('imagen_destacada')) {
        
      File::Delete('imagenes_destacadas/'.$new->imagen_destacada);
        
         $file = $request->file('imagen_destacada');
         $imagen_titulo = time().'.'.$file->getClientOriginalExtension();
         $file->move('imagenes_destacadas/', $imagen_titulo);
      }
      
      $new->publicacion = $request->publicacion;
      $new->destacada = $request->destacada;
      $new->portada = $request->portada;
   
      $publicacion = $request->has('publicacion') ? true : false;
      $destacada = $request->has('destacada') ? true : false;

      $imagen_definido = isset($imagen_titulo)? $imagen_titulo : $new->imagen_destacada;

      DB::table('news')->where('id', $new->id)
      ->update([
         'titulo' => $request->titulo,
         'descripcion' => $request->descripcion,
         'categoria_id' => $request->categoria_id,
         'publicacion' => $new->publicacion,
         'destacada' => $new->destacada,
         'portada' => $new->portada,
         'imagen_destacada' => $imagen_definido
         ]);
   
   	session()->flash('message', 'Noticia Editada');
   	return redirect()->route('path_noticias');
   }



   public function delete(News $new){
   	if (File::Delete('imagenes_destacadas/'.$new->imagen_destacada)) {
      $new->delete(); 
    }
      return redirect()->route('path_noticias')->with('message', 'registro eliminado');
      /*En caso de que haya mas usuarios..
      if ($new->user_id != $auth->user()->id) {
         return redirect()->route('path_noticias');.
      }*/
      /*
      alternative
      $r = News::finorFail($id);
      $result = $r->delete($r);*/	
   }


}

