<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news_month = News::select('titulo')
                   ->whereRaw('MONTH(created_at) = MONTH(now())')
                   ->count();
        $news_week = News::select('titulo')
                    ->whereRaw('WEEK(created_at) = WEEK(now())')
                    ->count();
        $news_day = News::select('titulo')
                    ->whereRaw('DAY(created_at) = DAY(now())')
                    ->count();

        $news_out = News::select('titulo')
                    ->count();

        $news_month_each = DB::table('news')
             ->select(DB::raw("DATE_FORMAT(created_at, '%m') as month, COUNT(id) as total"))
             ->groupBy(DB::RAW("DATE_FORMAT(created_at, '%m')"))
             ->get();

        
        
        return view('dashboard.home')->with(['news_out'=>$news_out, 'news_day'=>$news_day, 'news_week'=>$news_week, 'news_month'=>$news_month, 'month_each'=>$news_month_each]);

    }
}
