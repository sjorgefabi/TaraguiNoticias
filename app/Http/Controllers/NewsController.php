<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Categories;
use DB;
use File;
use Auth;
use App\Ads;
use Response;
use Carbon\Carbon;
use App\Http\Requests\CreateNewsRequest;
use App\Http\Requests\UpdateNewsRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
class NewsController extends Controller
{

    public function index(){
        
        $banner_hor1 = DB::table('portada')->select('contenido')->where('id', 1)->first();
        $banner_hor2 = DB::table('portada')->select('contenido')->where('id', 2)->first();
       
    	$news = News::with('categories')
                ->where('publicacion', 1)
                ->orderBy('created_at', 'desc')
                ->paginate(10);
                
        $cantNews = News::select('titulo')
                    ->count();
                    
        //lo mas leido
        $wread = DB::table('news')
                 ->join('categories', 'news.categoria_id', '=', 'categories.id')
                 ->select('news.titulo','news.created_at','news.imagen_destacada', 'news.id', 'categories.descripcion')
                 ->where('visualizacion','<>','0')
                 ->whereRaw('YEARWEEK(news.created_at) = YEARWEEK(now())')
                 ->orderBy('visualizacion', 'desc')
                 ->take(4)
                 ->get();

        //Noticias de portada
        $news_timeline = News::with('categories')
                ->where([
                ['publicacion', '=', 1],
                ['destacada', '=', 1]
                ])
                ->orderBy('created_at', 'desc')
                ->take(3)
                ->get();

  
      return view('News.index')
        ->with([
            'news'=>$news, 'wread'=>$wread, 
            'banner_hor1'=>$banner_hor1->contenido, 
            'banner_hor2'=>$banner_hor2->contenido,
            'cantNews'=>$cantNews, 
            'news_timeline'=>$news_timeline
        ]);
    }
    
    //imagen cabecera
    public function imgHeader(Request $request)
    {
        $this->validate($request, [
            'inp-img-header' => 'image|mimes:jpeg,jpg'
        ]);

        $img_save = false;
        $path = public_path('imagen_cabecera');
        $file_prev = $request->input('prev-inp-img-header');
        //se trae la ruta del arch ant. y se elimina, luego se almacena uno nuevo...
        if(File::exists($path))
        {
            File::Delete('imagen_cabecera.jpg');
        }
        if ($request->hasFile('inp-img-header'))
        {
            $file = $request->file('inp-img-header'); 
            $imagen_titulo = 'imagen_cabecera.'.$file->getClientOriginalExtension();
            $file->move('imagen_cabecera/', $imagen_titulo); 
            $img_save = true; 
        }
               
        if ($img_save)
            {
               return redirect()->route('new_path')->with('message', 'nueva imagen de cabecera guardada');
            }
        else
            {
                return redirect()->route('new_path')->withErrors('Hubo un error al intentar cargar el archivo');
            }
    }

    //buscador
    public function query(Request $request){
        $search = News::search($request->input('q'))->paginate(4);

        $cantNewsSearch = News::search($request->input('q'))->count();
        
        

        return view('News.resultsearch')->with(['result'=>$search, 'cantNewsSearch'=>$cantNewsSearch]);
    }

 //banner 1 y 2 ///////////////////////////////////////
    public function banner(Request $request){
           $img_before = DB::table('portada')
               ->select('contenido')
               ->where('id' ,1)
               ->first();
           
           File::Delete('imagenes_anuncios/'.$img_before->contenido);
        if ($request->hasFile('banner_img')) {
            $file = $request->banner_img; 
            $imagen_titulo = $request->banner_tit.'.'.$file->getClientOriginalExtension();

            $file->move('imagenes_anuncios/', $imagen_titulo);  
       }
       $banner1 = DB::table('portada')
                            ->where('id', 1)
                            ->update(['contenido'=> $imagen_titulo]);

            if ($banner1)
            {
               return redirect()->route('new_path')->with('message', 'nuevo banner horizontal(Nº1) guardado');
            }
            else{
                return redirect()->route('new_path')->withErrors($banner1);
            } 

    }
    public function banner2(Request $request){
           $img_before = DB::table('portada')
               ->select('contenido')
               ->where('id' ,2)
               ->first();
           
           File::Delete('imagenes_anuncios/'.$img_before->contenido);
        if ($request->hasFile('banner_img2')) {
            $file = $request->banner_img2; 
            $imagen_titulo = $request->banner_tit2.'.'.$file->getClientOriginalExtension();

            $file->move('imagenes_anuncios/', $imagen_titulo);  
     
       }
       $banner2 = DB::table('portada')
                            ->where('id', 2)
                            ->update(['contenido'=> $imagen_titulo]);

            if ($banner2)
            {
               return redirect()->route('new_path')->with('message', 'nuevo banner horizontal(Nº2) guardado');
            }
            else{
                return redirect()->route('new_path')->withErrors($banner2);
            } 

    }

/***********
Categorias
***********/
	 public function categoria($name){
        $categid = Categories::select('id')->where('descripcion', $name)->firstOrFail();
        
       // dd($categid->id);
        $news = News::where('categoria_id', $categid->id)
                           ->orderBy('created_at', 'desc')
                            ->paginate(5);

        $cantNewsCateg = News::select('titulo')
                               ->where('categoria_id', $categid['id'])
                               ->count();

        $sharefab = 0;
        $sharetwi = 0;
        $sharewha = 0;
        $shareimg = 0;
     
    	return view('News.categorias.index')->with(['news'=>$news, 'namecateg' => $name,
        'sharefab'=>$sharefab, 'sharetwi'=>$sharetwi, 'sharewha'=>$sharewha, 'cantNewsCateg'=>$cantNewsCateg, 'shareimg'=>$shareimg ]);
	}
    
    public function show_categoria($name, $id){
        //detalle noticia con su id de la categoria correspondiente
        $new = News::FindOrFail($id);
        //si existe la noticia y no hay usuario autentificado contar visita
        if (!is_null($new)) {
            if (!Auth::check()) {
              $new->visualizacion++;
              $new->save();
           }
        }
        //para compartir redes sociales
        return view('News.categorias.show')->with(['new'=>$new]);
    }

    public function getNewsHistory(){
         //historial de noticias
        $history_news = DB::table('news')
         ->select(DB::raw('year(created_at) as year,  DATE_FORMAT(created_at, "%M") as month_name, DATE_FORMAT(created_at, "%m") as month_num, 
         count(month(created_at)) as count_x_month'))
         ->groupby(DB::raw('YEAR(created_at), DATE_FORMAT(created_at, "%M"), DATE_FORMAT(created_at, "%m")'))
         ->orderby('created_at', 'ASC')
         ->get();
       
        $h_n = $history_news->groupBy('year');

        return view('News.historialNoticias._historialIndex')
        ->with(['h_n' => $h_n]);
    }

    public function getNewsYear($year){
        $newsYear = News::with('categories')
                ->where('publicacion', 1)
                ->whereRaw('year(created_at) = '.$year)
                ->orderBy('created_at', 'desc')
                ->paginate(10);

        $cantNewsYear = News::select('titulo')
                ->whereRaw('year(created_at)', $year)
                ->count();

        return view('News.historialNoticias.historialAnio')->with(['newsYear'=>$newsYear,
        'year'=>$year, 'cantNewsYear'=>$cantNewsYear]); 
    }

    public function getNewsMonthYear($month, $year){
        $newsMonthYear = News::with('categories')
                ->where('publicacion', 1)
                ->whereRaw('year(created_at) = '.$year)
                ->whereRaw('month(created_at) ='. $month)
                ->orderBy('created_at', 'desc')
                ->paginate(10);
        $cantNewsMonthYear = News::select('titulo')
                ->whereRaw('year(created_at) = '.$year)
                ->whereRaw('month(created_at) ='. $month)
                ->count();
        
        return view('News.historialNoticias.historialMesAnio')->with(['newsMonthYear'=>$newsMonthYear,
        'year'=>$year, 'month'=>$month, 'cantNewsMonthYear'=>$cantNewsMonthYear]);
    }


    
}
