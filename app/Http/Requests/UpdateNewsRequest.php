<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateNewsRequest extends CreateNewsRequest
{
    public function authorize(){
    	/* en caso de que haya mas usuarios y solo edite el que lo creo
    	el metodo iguala el id del usuario logueado (usado gracias a request)
    	y se obtiene una instancia de la noticia (model biding en la ruta)
    	para usar el campo user_id*/
    	return $this->user()->id = $this->new->user_id; 
    	
    }

    public function rules()
    {
        return [
            'titulo' => 'required',
            'descripcion' => 'required',
            'categoria_id' => 'required|not_in:0',
            //'imagen_destacada' => 'required|image|mimes:jpeg,jpg,png|max:2048',
            'portada' => '1_portada',
            
            /*'create_new' => 'date_format: d-m-Y',*/
            /*'portada' => 'unique_with:news,portada',
            'portada' => 'exists:news',*/
        ];
    }
}
