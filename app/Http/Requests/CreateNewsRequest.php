<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo' => 'required',
            'descripcion' => 'required',
            'categoria_id' => 'required|not_in:0',
            'imagen_destacada' => 'required|image|mimes:jpeg,jpg,png|max:2048',
            'portada' => '1_portada',
            
            /*'create_new' => 'date_format: d-m-Y',*/
            /*'portada' => 'unique_with:news,portada',
            'portada' => 'exists:news',*/
        ];
    }
}
