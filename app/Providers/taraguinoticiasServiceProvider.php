<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\News;
use App\Ads;
use App\Categories;
use Carbon\Carbon;
//use DB;
//para multiples vistas
use Illuminate\Support\Facades\View;

class taraguinoticiasServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
         // Using class based composers...
        View::composer(
            'profile', 'App\Http\ViewComposers\ProfileComposer'
        );
        // Using Closure based composers...
        View::composer('layout.app', function ($view) {
             $ads = Ads::select('imagen')->get();
        //descripcion de fecha
        $date = Carbon::now();
        $weekday = News::$weekMap[$date->dayOfWeek];
        $month = News::$monthMap[$date->month];

        //para navbar
        $categorias = Categories::select('descripcion')->get();

        $view->with(['ads'=>$ads, 'date'=>$date, 'weekday'=>$weekday, 'month'=>$month, 
        'categorias'=>$categorias]);
        
        });

       /* View::composer('layout.app', function($view) {
            
            $view->with(['categorias'=>$categorias]);
        });*/

        /*View::composer('layout._footer', function($view) {
            //para navbar
            $categorias = Categories::select('descripcion')->get();
            $view->with(['categorias'=>$categorias]);
        });*/


    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
