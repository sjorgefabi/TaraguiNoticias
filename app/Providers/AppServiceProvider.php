<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema; //Import Schema
use Validator;

use App\News;
use App\Ads;
use App\Categories;
use Carbon\Carbon;
use File;
use DB;
/*use Carbon\Carbon;
use DB;*/
//para multiples vistas
use Illuminate\Support\Facades\View;
//use Jenssegers\Date\Date;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    public function boot()
    {
        
    
        Schema::defaultStringLength(191); //Solved by increasing StringLength
        /*
            |--------------------------------------------------------------------------
            | view composer
            |--------------------------------------------------------------------------
            |
            | composer se encargara de distribuir la variable a las vistas que la comparten
            |
         */

         // Using class based composers...
        View::composer(
            'profile', 'App\Http\ViewComposers\ProfileComposer'
        );
        // Using Closure based composers...
        View::composer('layout.app', function ($view) {
            $ads = Ads::select('imagen')->get();
        //descripcion de fecha
        $date = Carbon::now();
        $weekday = News::$weekMap[$date->dayOfWeek];
        $month = News::$monthMap[$date->month];

        //para navbar
        $categorias = Categories::select('descripcion')->get();

         /*imagen de cabecera
               se definen: 
                aux: p/nombre del archivo,
                manuals p/areglo que contendra los datos del archivo,
                path del dir
                y archivo/s presentes en el dir,
               -condicion:
               si no existe el archivo en el dir, aux queda vacio (default)
               si no, se recorre los arch existentes en el dir, se obtienen sus datos y luego
                se almacena el nombre del dir y arch con los datos necesarios
               **/  
              $aux = '';
              $manuals = [];
              $path = public_path('imagen_cabecera');
              $filesInFolder = File::files('imagen_cabecera');
              //$files = File::allFiles('imagenes_destacadas');
        
              if(!File::exists($path))
               {
                   $aux;
               }
               else
               {
                   foreach($filesInFolder as $file)
                   {
                      //$manuals[] = pathinfo($file);

                       $pInfo = pathinfo($file);
                       
                       if(! empty($pInfo) && is_array($pInfo)){
                          $manuals = $pInfo;
                       }
                   }
                   $aux =  $manuals['basename'];
               }
               $imagen_cabecera = $aux;
                                   
             $view->with(['ads'=>$ads, 'date'=>$date, 'weekday'=>$weekday, 'month'=>$month, 
            'categorias'=>$categorias, 'imagen_cabecera'=>$imagen_cabecera]);  
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
       
    }
}
