<?php

namespace App;

use App\News;
use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
   protected $table = 'categories';
   protected $fillable = ['descripcion'];

  public function news(){
  	return $this->hasToMany(News::class);
  }

}
