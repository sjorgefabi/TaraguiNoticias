<?php

namespace App;

use App\user;
use App\Categories;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
   protected $table = 'news';
   protected $fillable = ['titulo', 'descripcion', 'categoria_id', 'imagen_destacada','publicacion', 'destacada', 'portada'];
   //protected $guarded = ['publicacion', 'destacada'];

   public static $weekMap = [
           0 => 'Domingo',
           1 => 'Lunes',
           2 => 'Martes',
           3 => 'miercoles',
           4 => 'Jueves',
           5 => 'Viernes',
           6 => 'Sábado'
        ];

    public static $monthMap = [
            1 => 'Enero',
            2 => 'Febrero',
            3 => 'Marzo',
            4 => 'Abril',
            5 => 'Mayo',
            6 => 'Junio',
            7 => 'Julio',
            8 => 'Agosto',
            9 => 'Septiembre',
            10 => 'Octubre',
            11 => 'Noviembre',
            12 => 'Diciembre'
    ];
   
   /*mutattors: eloquent modifica valor antes de ser asignado, util para checkbox
   en controller: $new->publicacion = $request->publicacion (on = 1)*/
   public function setPublicacionAttribute($value){
       $this->attributes['publicacion'] = ($value == 'on') ? true : false;
   }

   public function setDestacadaAttribute($value){
      $this->attributes['destacada'] = ($value == 'on') ? true : false;
   }

   public function setPortadaAttribute($value){
      $this->attributes['portada'] = ($value == 'on') ? true : false;
   }

   //eloquent relaciones
   public function user(){
    	return $this->belongsTo(User::class);
   }
   
   public function categories(){
     return $this->belongsTo(Categories::class, 'categoria_id')->select('id', 'descripcion');
   }
   
   //scope para buscador
   public function scopeSearch($query, $titulo)
   {
    //if (trim($titulo) != "") {
       return $query->where('titulo', 'like', '%'. $titulo .'%');
    //}
   }

}

