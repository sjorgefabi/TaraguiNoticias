@extends('layout.app')
@section('content')
{{-- form para crear o editar --}}
@if ($new->exists)
	 <form action="{{ route('actualizarnoticia', ['new'=>$new->id])}}" method="POST" role="form">
	{{method_field('PUT')}}
	<legend>Editar Noticia</legend>	
@else
   <form action="{{ route('guardarnoticia') }}" method="POST" role="form">
	<legend>Crear nueva noticia</legend>
@endif
    {{csrf_field()}}
		<div class="form-group">
			<span class="label label-default"">Titulo</span>
			<input type="text" name="titulo" id="titulo" class="form-control" value="{{$new->titulo or old('titulo')}}"  title="titulo">
		</div>
		<div class="form-group">
			<span class="label label-default"">Descripcion</span>
			<input type="text" name="descripcion" id="descripcion" class="form-control" value="{{$new->descripcion or old('descripcion')}}" required="required" title="descripcion">
		</div>
	<button type="submit" class="btn btn-primary">Guardar</button>
</form>
@endsection