@extends('layout.app')
@section('content')
<div class="col-md-9 col-xs-9">
        @foreach($h_n as $year=>$keys)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{ route('getNewsYear', ['year'=>$year]) }}"><b>{{ $year}}</b> ({{ ($keys->sum('count_x_month')) }}) </a>
                    </div>
                    <div class="panel-body">
                            @foreach($keys as $key)
                                <a href="{{ route('getNewsMonthYear', ['month'=>$key->month_num, 'year'=>$year]) }}" class="btn btn-link"><strong>{{ $key->month_name }}</strong>({{ $key->count_x_month  }})</a>
                            @endforeach
                    </div>
                </div>
         @endforeach

          
</div>

@endsection


