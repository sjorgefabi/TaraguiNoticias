@extends('layout.app')
  @section('facebook_meta')
        <meta property="og:updated_time" content="{{ time() }}" />

        <meta property="og:title"         content= 'Taragui Noticias' />

        <meta property="og:url"           content='http://www.taraguinoticias.com' />

        <meta property="og:description"   content= 'Taragui Noticias, portal de noticias en Corrientes'/>

        <meta property="og:type"          content='website' />

       <meta property="og:image"       content= '{{ url('css/banner_1_taraguiNoticias.png') }}' />

      <meta property="og:image:alt"       content= 'logo_1_taragui_noticias'/>

  @endsection
  @section('twitter_meta')
        <meta name="twitter:card" content="summary" />

        <meta name="twitter:site" content='http://www.taraguinoticias.com' />

        <meta name="twitter:creator" content="@nickbilton" />

        <meta name="twitter:title" content= 'Taragui Noticias' />

        <meta name="twitter:description" content= 'Taragui Noticias, portal de noticias en Corrientes' />

    <meta name="twitter:image" content= '{{ url('css/banner_1_taraguiNoticias.png') }}' />
  @stop

@section('content')
   <style type="text/css" media="screen">
    .carousel-caption {
      position: relative;
      left: 0%;
      right: 0%;
      bottom: 0px;
      z-index: 10;
      padding-top: 0px;
      padding-bottom: 0px;
      color: #000;
      text-shadow: none;
      & .btn {
        text-shadow: none; // No shadow for button elements in carousel-caption
      }
    }

    .carousel {
      position: relative;
    }

    .controllers {
      position: absolute;
      top: 0px;
    }

    .carousel-control.left, 
    .carousel-control.right {
      background-image: none;
    }
  </style>
  
 {{-- carousel --}}
 @if ($news_timeline)
  <div id="carousel-example-generic" class="carousel slide container" data-ride="carousel">
    <!-- Wrapper for slides -->
      <div class="carousel-inner">
      @foreach ($news_timeline as $new)

        <div @if ($loop->first) class="item active" @endif class="item">
          <div class="holder col-sm-8">
           <span class="label label-default" style="font-size: 12px; border-radius: none;">
            {{ $new->categories->descripcion }}
          </span>
            <img style="height: 400px; width: 1000px; border: 1px solid rgb(248,248,248);" class="img-responsive social-share-img{{ $shareimg++ }}" src="{{ url('imagenes_destacadas/'.$new->imagen_destacada) }}" alt="imagen_portada">
          </div>

          <div class="col-sm-4">
            <div class="carousel-caption">
              <a href="{{ route('cat_detalle', ['name'=>$new->categories->descripcion,'new'=>$new->id]) }}" title="{{ $new->titulo }}">
                  <h3 style="color: #424242!important;">
                    {{ str_limit( $new->titulo, $limit = 100, $end = '...') }}
                  </h3>
              </a>
              
              @if ( strlen($new->titulo) >= 50 )
                <p>
                  {!! str_limit( $new->descripcion, $limit = 300, $end = '...') !!}
                </p>
              @else
                <p>
                  {!! str_limit( $new->descripcion, $limit = 500, $end = '...') !!}
                </p>
              @endif

              <a href="{{ route('cat_detalle', ['name'=>$new->categories->descripcion,'new'=>$new->id]) }}" title="{{ $new->titulo }}" class="btn btn-default">
                 Ver más
              </a>


                
            </div>
          </div>
        </div>
      @endforeach
     
    </div>{{-- carousel-inner --}}
    
    
    <div class="controllers col-sm-8 col-xs-12">
      <!-- Controls -->
      <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
      </a>
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
      </ol> 
    </div>
{{-- carousel --}}
@endif
      
        <div class="col-md-9">
            @if (Auth::check())
              <div class="col-md-12 collapse" id="collapseImg-ads-hor-1">
                 <form action="{{ route('new_banner_hor') }}" method="POST" role="form" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  {{ method_field('PUT') }}
                        <div class="form-group">
                            <div class="pull-right btns-ban-h1">
                               <button type="button" class="btn btn-default" id="btn-cancel-ban-h1">Cancelar</button>
                               <button type="submit" id="save-banner-horizontal" class="btn btn-primary">Confirmar</button>
                            </div> 
                            <input id="banner_tit" name="banner_tit" class="form-control" type="text"  placeholder="titulo banner" required>                        
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-success btn-file">
                                        Seleccionar archivo… <input type="file" name ="banner_img" id="banner_img">
                                    </span>
                                </span>
                                <input type="text" class="form-control" id="inp-banner-h1" readonly>
                            </div>
                        </div>
                 </form>         
              </div>
          @endif
        <hr>
        <!-- Load Facebook SDK for JavaScript -->
          <div class="row">{{-- row nivel2 --}}
               <div class="col-md-12">{{-- ads  --}}
                  @if (Auth::check())
                    <button style="position: absolute" class="btn btn-warning btn-md" data-toggle="collapse" data-target="#collapseImg-ads-hor-1" aria-expanded="false" aria-controls="collapseImg-ads-hor-1">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </button>
                    <img id='img-upload-bh1-new' style="height: 200px; display: none;" class="thumbnail img-responsive" width="845" src="#" alt="banner_horizontal_1_new">
                  @endif
                  <img id='img-upload' style="height: 200px; display: block;" class="thumbnail img-responsive" width="845" src="{{ url('imagenes_anuncios/'.$banner_hor1) }}" alt="banner_horizontal_1"><hr>
                </div>
                <div class="col-md-12">{{-- col-md-12 content news index --}}

                  <div class="card">
                    <div class="grid-sizer  col-sm-2 col-md-2"></div>{{-- 2 col --}}
                    <input hidden id="cantNews" type="text" value="{{ $cantNews }}">
                    @foreach ($news as $new)
                      <div class="grid-item col-xs-12 col-sm-12 col-md-6">{{-- 2 div --}}
                          <!-- add inner element for column content -->
                          <a href="{{ route('cat_detalle', ['name'=>$new->categories->descripcion,'new'=>$new->id]) }}" title="{{$new->titulo}}">
                            <h3 style="color: #424242!important;">{{$new->titulo}}</h3>
                          </a>
                             <div class="card-image">   
                                <img  height="650" class="img-responsive social-share-img{{ $shareimg++ }}" src="{{ url('imagenes_destacadas/'.$new->imagen_destacada) }}">
                              </div> 
                              <div style="border-left: 4px solid #f0f0f0">
                                  <div class="card-action">
                                        {{--<i class="glyphicon glyphicon-calendar" aria-hidden="true"></i> {{ Carbon\Carbon::parse($new->created_at)->formatLocalized('%d/%m/%Y')}}--}}
                                        <i class="glyphicon glyphicon-bookmark" aria-hidden="true"></i>{{ $new->categories->descripcion or 'null'}}
                                      <div class="pull-right btn-group" style="margin-top: -8px;">                        
                                        <a class="btn btn-sm share{{ $sharefab++ }} s_facebook" href="#"  data-text="{{ $new->titulo}}" data-link="{{ url('/Categoria/'.$new->categories->descripcion.'/'.$new->id)}}">
                                          <i class="fa fa-facebook-square fa-2x"></i>
                                        </a>

                                        <a style="color: #40c4ff!important;" class="btn btn-sm share{{ $sharetwi++ }} s_twitter" href="#" target="new_blank" data-text="{{ $new->titulo}}" data-link="{{ url('/Categoria/'.$new->categories->descripcion.'/'.$new->id)}}">
                                          <i class="fa fa-twitter-square fa-2x"></i>
                                        </a>
                                        <a style="color: #4caf50!important;" class="btn btn-sm visible-xs share{{ $sharewha++ }} s_whatsapp" data-text="{{ $new->titulo}}" data-link="{{ url('/categoria/'.$new->categories->descripcion.'/'.$new->id)}}"><i class="fa fa-whatsapp fa-2x" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                  </div><!-- card actions -->
                                </div><hr>
                                  {{-- <div class="card-content">
                                      <p class="block-with-text">{!! str_limit($new->descripcion, '300','...') !!}</p><hr>       
                                  </div> card content --}}
                      </div> {{-- 2 div --}}
                    @endforeach
                    
                  </div>{{-- card1 --}}
                     <div class="text-center">
                      {{ $news->render() }}
                    </div>
                    <hr>
                </div>{{-- col-md-12 content news index--}}
            @if (Auth::check())
              <div class="col-md-12 collapse"  id="collapseImg-ads-hor-2">
                 <form action="{{ route('new_banner_hor2') }}" method="POST" role="form" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  {{ method_field('PUT') }}
                        <div class="form-group">
                           <div class="pull-right" id="btns-ban-h2">
                              <button type="button" class="btn btn-default" id="btn-cancel-ban-h2">Cancelar</button>
                              <button type="submit" id="save-banner-horizontal" class="btn btn-primary">Confirmar</button>
                           </div>
                            <input id="banner_tit2" name="banner_tit2" class="form-control" type="text"  placeholder="titulo banner2" required>                          
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-success btn-file">
                                        Seleccionar archivo… <input type="file" name ="banner_img2" id="banner_img2">
                                    </span>
                                </span>
                                <input type="text" class="form-control" id="inp-banner-h2" readonly>
                            </div>
                        </div>
                 </form>     
              </div>{{-- ads --}}
              <hr>
             @endif
                <div class="col-md-12">{{-- ads--}}
                    @if (Auth::check())
                      <button style="position: absolute" class="btn btn-warning btn-md" data-toggle="collapse" data-target="#collapseImg-ads-hor-2" aria-expanded="false" aria-controls="collapseImg-ads-hor-2">
                          <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                      </button>
                      <img id='img-upload-bh2-new' style="height: 150px; display: none;" class="thumbnail img-responsive" width="845" src="#" alt="banner_horizontal_2_new">
                    @endif
                    <img id="img-upload2" style="height: 150px; display: block;" class="thumbnail img-responsive" src="{{ url('imagenes_anuncios/'.$banner_hor2) }}"  width="845" alt="banner2_horizontal">   
               </div>{{-- end ads --}}
               @if (!$wread->isEmpty())
                 <div class="col-md-12">
                 <div class="well well-sm well-text-sections">Populares de la semana</div>
               </div>
               @endif
               @foreach ($wread as $data)
                 <div class="col-md-3">{{-- content - populares de la semana--}}
                    <div class="thumbnail">
                      <i class="glyphicon glyphicon-calendar" aria-hidden="true"></i> {{ Carbon\Carbon::parse($data->created_at)->formatLocalized('%d/%m/%Y') }}
                       <img src="{{ url('imagenes_destacadas/'.$data->imagen_destacada) }}" alt="preview_image_news_view_most">
                        <div class="caption">
                            <h5><a href="{{ route('cat_detalle', ['name'=>$data->descripcion,'new'=>$data->id])}}" title="title_news">{{ $data->titulo }}</a></h5>
                        </div>
                      </div>        
                </div>
              @endforeach

            </div><!--row nivel 2-->
    </div>{{-- col 9 --}}
          
      
@endsection