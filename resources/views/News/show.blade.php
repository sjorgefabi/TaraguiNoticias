@extends('layout.app')
@section('content')
  <div class="col-md-9">
  	<legend><h1>{{ $new->titulo }}</h1></legend>
  	 <b>Autor</b> {{ $new->user->name }} 
	  <p id="texto-detalle-noticia">{{ $new->descripcion }}
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	 </p>
		<hr>
		   <a class="url" href="{{ route('new_path') }}" role="button">volver</a>
  </div>
  	<aside>
  		@include('layout.aside-ads')
  	</aside>
	 
	 <style>
	 	#texto-detalle-noticia::first-letter {
 			 font-size: 2em;
 			 text-transform:uppercase;
		}
	 </style>
@endsection