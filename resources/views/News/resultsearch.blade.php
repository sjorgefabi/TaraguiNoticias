@extends('layout.app')
@section('content')
	 <div class="col-md-9 col-xs-9 target-resp">
       <div class="row row-margin-bottom">
            <div class="col-md-11 col-xs-12 no-padding lib-item" data-category="view">
              <input hidden id="cantNewsSearch" type="text" value={{ $cantNewsSearch }}>
              <h3 class="page-header">Resultados de la busqueda ...</h3>
                <div class="lib-panel">{{-- panel1 --}}
                  @forelse ($result as $data)
                    <div class="row box-shadow">
                        <div class="col-md-4">
                            <a href="{{ route('cat_detalle', ['name'=>$data->categories->descripcion, 'id'=>$data->id]) }}">
                                <img style="height: 200px;" class="lib-img-show social-share-imgSearch{{ $shareimg++ }}" src="{{ url('imagenes_destacadas/'.$data->imagen_destacada) }}">
                            </a>
                        </div>	
                        <div class="col-md-8">
                        	  <div class="lib-row lib-header">
                                <h3><a style="color: #424242!important;" href="{{ route('cat_detalle', ['name'=>$data->categories->descripcion, 'id'=>$data->id]) }}" title="titulo">{{ str_limit($data->titulo , 150, ' ...')}}</a></h3>
                                <div class="lib-header-seperator"></div>
                                {{-- fecha, categoria --}}
                                <p style="font-size: 0.53em; color: #DCDCDC">
                                  <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> {{ Carbon\Carbon::parse($data->updated_at)->formatLocalized('%d/%m/%Y') }}
                                <span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span> {{ $data->categories->descripcion or 'null' }}
                          </div>
                                <div class="pull-right btn-group">
                                    <a class="btn btn-sm shareNewsSearch{{ $sharefab++ }} s_facebook" href="#"  data-text="{{ $data->titulo}}" data-link="{{ url('/Categoria/'.$data->categories->descripcion.'/'.$data->id)}}">
                                              <i class="fa fa-facebook-square fa-2x"></i>
                                    </a>
    
                                    <a class="btn btn-sm shareNewsSearch{{ $sharetwi++ }} s_twitter" style="color: #40c4ff!important;" href="#" target="new_blank" data-text="{{ $data->titulo}}" data-link="{{ url('/Categoria/'.$data->categories->descripcion.'/'.$data->id)}}">
                                              <i class="fa fa-twitter-square fa-2x"></i>
                                    </a>
    
                                    <a class="btn btn-sm visible-xs shareNewsSearch{{ $sharewha++ }} s_whatsapp" data-text="{{ $data->titulo}}" data-link="{{ url('/categoria/'.$data->categories->descripcion.'/'.$data->id)}}"><i class="fa fa-whatsapp fa-2x" aria-hidden="true"></i>
                                    </a>
                                </div>
                        </div>
                    </div><br>{{-- box shadown --}}
                    @empty
                    <div>
                    	No se encontraron datos relacionados..
                    	
                    </div>
                    @endforelse
                </div>
                {{ $result->render() }}
            </div>{{-- nivel2 --}}
           </div>
     </div>{{-- nivel 1 --}}
	 
     
@endsection