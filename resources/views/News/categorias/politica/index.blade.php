@extends('layout.app')
@section('content')

     <div class="col-md-9">
       <div class="row row-margin-bottom">
            <div class="col-md-11 no-padding lib-item" data-category="view">

              <h3 class="page-header">{{ $namecateg }}</h3>
                <div class="lib-panel">{{-- panel1 --}}
                  @foreach ($news as $new)
                    <div class="row box-shadow">

                        <div class="col-md-4">
                            <img class="lib-img-show" src="{{ url('imagenes_destacadas/'.$new->imagen_destacada) }}" height="200" width="100">
                        </div>	

                        <div class="col-md-8">
                        	  <div class="lib-row lib-header">
                                <h3><a href="{{ route('cat_politica_detalle', ['name'=>$new->titulo, 'id'=>$new->id]) }}" title="titulo">{{ $new->titulo}}</a></h3>
                                <div class="lib-header-seperator"></div>
                                {{-- fecha, caegoria --}}
                                <p style="font-size: 0.53em; color: #DCDCDC">
                                  <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> {{ Carbon\Carbon::parse($new->updated_at)->toFormattedDateString() }}
                                <span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span> {{ $new->categories->descripcion or 'null' }}
                                                    	
                          </div>
                              <div class="lib-row lib-desc">
                                {{ str_limit($new->descripcion,'300','...') }}
                              </div>
                              <a class="pull-right btn btn-link" href="{{ route('cat_politica_detalle', ['id' => $new->id]) }}" title="Ver Mas">Leer más</a>
                        	
                        </div>
                    </div><br>{{-- box shadown --}}
                    @endforeach
                </div>
                {{ $news->render() }}
            </div>{{-- nivel2 --}}
           </div>
     </div>{{-- nivel 1 --}}
	 
     <style type="text/css" media="screen">
     	.lib-panel {
		    margin-bottom: 20px;
		}
		.lib-panel img {
		    width: 100%;
		    background-color: transparent;
		}

		.lib-panel .row,
		.lib-panel .col-md-4 {
		    padding: 0;
		    background-color: #FFFFFF;
		}


		.lib-panel .lib-row {
		    padding: 0 20px 0 20px;
		}

		.lib-panel .lib-row.lib-header {
		    background-color: #FFFFFF;
		    font-size: 20px;
		    padding: 2px 20px 0 20px;
		}

		.lib-panel .lib-row.lib-header .lib-header-seperator {
		    height: 2px;
		    width: 140px;
		    background-color: #d9d9d9;
		    margin: 1px 0 1px 0;
		}

		.lib-panel .lib-row.lib-desc {
		    position: relative;
		    height: 100%;
		    display: block;
		    font-size: 13px;
		}
		.lib-panel .lib-row.lib-desc a{
		    position: absolute;
		    width: 100%;
		    bottom: 10px;
		    left: 20px;
		}

		.row-margin-bottom {
		    margin-bottom: 20px;
		}

		.box-shadow {
		    -webkit-box-shadow: 0 0 10px 0 rgba(0,0,0,.10);
		    box-shadow: 0 0 10px 0 rgba(0,0,0,.10);
		}

		.no-padding {
		    padding: 0;
		}
     </style>
     <aside>
        @include('layout.aside-ads')
     </aside>
@endsection