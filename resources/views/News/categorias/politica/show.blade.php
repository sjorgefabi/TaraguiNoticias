@extends('layout.app')
@section('content')
  <div class="col-md-9">
  	<h3>{{ $new->titulo }}</h3>
	<div class="bg-warning col-md-4">
	<span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> {{ Carbon\Carbon::parse($new->updated_at)->toFormattedDateString()}}
    <span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span> {{ $new->categories->descripcion or null}}
	</div><br>
	<article>
		{!! $new->descripcion or null!!}
	</article>
  </div>
  <aside>
	  @include('layout.aside-ads')
  </aside>
@endsection