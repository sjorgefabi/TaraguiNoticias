@extends('layout.app')
@section('content')
     <div class="col-md-9 col-xs-9 target-resp">
       <div class="row row-margin-bottom">
            <div class="col-md-11 col-xs-12 no-padding lib-item" data-category="view">
              <input hidden id="cantNewsCateg" type="text" value={{ $cantNewsCateg }}>
              <h3 id="cat-header" class="page-header">{{ $namecateg }}</h3>
                <div class="lib-panel">{{-- panel1 --}}  
                  @forelse ($news as $new)
                    <div class="row box-shadow">
                        <div class="col-md-4">
                            <a href="{{ route('cat_detalle', ['name'=>$new->categories->descripcion, 'id'=>$new->id]) }}" title="detalle">
                            	<img class="lib-img-show social-share-imgcateg{{ $shareimg++ }}" src="{{ url('imagenes_destacadas/'.$new->imagen_destacada) }}">
                            </a>
                        </div>	
 
                        <div class="col-md-8">  
                             <div class="lib-row lib-header">
                                <h3><a style="color: #424242!important;" href="{{ route('cat_detalle', ['name'=>$new->categories->descripcion, 'id'=>$new->id]) }}" title="titulo">{{ $new->titulo}}</a></h3>
                                <div class="lib-header-seperator"></div>
                                {{-- fecha, caegoria --}}
                                <p style="font-size: 0.53em; color: #DCDCDC">
                                  <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> {{ Carbon\Carbon::parse($new->updated_at)->formatLocalized('%d/%m/%Y') }}
                                <span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span> {{ $new->categories->descripcion or 'null' }}
                                                    	
                              </div>
                              {{-- <div class="lib-row lib-desc">
                                {!! str_limit($new->descripcion,'300','...') !!}
                              </div>comment --}}

                              <div class="pull-right btn-group">
                                <a class="btn btn-sm shareNewsCateg{{ $sharefab++ }} s_facebook" href="#"  data-text="{{ $new->titulo}}" data-link="{{ url('/Categoria/'.$new->categories->descripcion.'/'.$new->id)}}">
                                          <i class="fa fa-facebook-square fa-2x"></i>
                                </a>

                                <a style="color: #40c4ff!important;" class="btn btn-sm shareNewsCateg{{ $sharetwi++ }} s_twitter" href="#" target="new_blank" data-text="{{ $new->titulo}}" data-link="{{ url('/Categoria/'.$new->categories->descripcion.'/'.$new->id)}}">
                                          <i class="fa fa-twitter-square fa-2x"></i>
                                </a>

                                <a style="color: #4caf50!important;" class="btn btn-sm visible-xs shareNewsCateg{{ $sharewha++ }} s_whatsapp" data-text="{{ $new->titulo}}" data-link="{{ url('/categoria/'.$new->categories->descripcion.'/'.$new->id)}}"><i class="fa fa-whatsapp fa-2x" aria-hidden="true"></i>
                                </a>
                             </div>
                        </div>
                    </div><br>{{-- box shadown --}}
                    @empty
                    <p>No se han registrado datos..</p>
                    @endforelse
                </div>
                {{ $news->render() }}
            </div>{{-- nivel2 --}}
           </div>
     </div>{{-- nivel 1 --}}
@endsection