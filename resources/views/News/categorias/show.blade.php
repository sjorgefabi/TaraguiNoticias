@extends('layout.app')
@section('facebook_meta')
      <meta property="og:updated_time" content="{{ time() }}" />

      <meta property="og:title"         content= '{{ $new->titulo }}' />

      <meta property="og:url"           content='http://www.taraguinoticias.com/{{ $new->categories->descripcion or null}}/{{ $new->id }}' />

      <meta property="og:description"   content= '{!! $new->descripcion or null !!}'/>

      <meta property="og:type"          content='website' />

     <meta property="og:image"       content= "{{ url('imagenes_destacadas/'.$new->imagen_destacada) }}" />

    <meta property="og:image:alt"       content= 'imagen-destacada'/>

@endsection
@section('twitter_meta')
      <meta name="twitter:card" content="summary" />

      <meta name="twitter:site" content='http://www.taraguinoticias.com/{{ $new->categories->descripcion or null}}/{{ $new->id }}' />

      <meta name="twitter:creator" content="@nickbilton" />

      <meta name="twitter:title" content='{{ $new->titulo }}' />

      <meta name="twitter:description" content='{!! $new->descripcion or null !!}' />

  <meta name="twitter:image" content='{{ url('imagenes_destacadas/'.$new->imagen_destacada) }}' />
@stop
@section('content')
  <div class="col-md-9">
  
  	<h3>{{ $new->titulo }}</h3>
  	<div style="width: 100%;">
  		<i class="glyphicon glyphicon-calendar" aria-hidden="true"></i> {{ Carbon\Carbon::parse($new->created_at)->formatLocalized('%d/%m/%Y') }}
  	    <i class="glyphicon glyphicon-bookmark" aria-hidden="true"></i> {{ $new->categories->descripcion or null}}                                                   
  	</div><br>
    <style>
      article{
        margin-top: 5px;
        font-size: 16px; 
        line-height: 28.4px;
      }
      article:first-letter{
        text-transform:uppercase;
        font-size: 25px;
      }
    </style>
    <img src="{{ url('imagenes_destacadas/'.$new->imagen_destacada) }}" alt="img-destacada-{{ $new->titulo }}" style="height: 400px; width: 100%;" class="img-responsive"
         title="img-destacada-{{ $new->titulo }}">
  	<article>
          {!! $new->descripcion or null !!}
  	</article>
	Comparte esta noticia en: 
  	<div class="btn-group"> 
  	      <a class="img-destaca-share-social" hidden href="{{ url('imagenes_destacadas/'.$new->imagen_destacada) }}" title="img-destaca-share-social"></a> 
                            
            <a class="btn btn-sm shareCategDetalle s_facebook" href="#"  data-text="{{ $new->titulo}}" data-link="{{ url('/Categoria/'.$new->categories->descripcion.'/'.$new->id)}}">
                <i class="fa fa-facebook-square fa-2x"></i>
             </a>
             <a style="color: #40c4ff!important;" class="btn btn-sm shareCategDetalle s_twitter" href="#" target="new_blank" data-text="{{ $new->titulo}}" data-link="{{ url('/Categoria/'.$new->categories->descripcion.'/'.$new->id)}}">
               <i class="fa fa-twitter-square fa-2x"></i>
             </a>
             <a style="color: #4caf50!important;" class="btn btn-sm visible-xs shareCategDetalle s_whatsapp" data-text="{{ $new->titulo}}" data-link="{{ url('/categoria/'.$new->categories->descripcion.'/'.$new->id)}}"><i class="fa fa-whatsapp fa-2x" aria-hidden="true"></i>
             </a>
    </div>

  </div>{{-- col md 9 --}}<hr class="visible-xs">
@endsection