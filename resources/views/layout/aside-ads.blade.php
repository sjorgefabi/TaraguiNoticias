<div class="col-md-3 col-xs-12" style="background-color: #f5f5f5; height: 100%!important; border-left: 2px solid #e0e0e0; top: 20px;">

  {{-- fecha y hora --}}
    <div class="text-center" style="margin-top: 10px;">
        <p>{{ $weekday.': '.$date->day.' de '.$month.', año:'.$date->year}}</p>
       <h4 style="font-weight: bold; color: #616161"><i class="fa fa-clock-o fa-2x" aria-hidden="true"></i> <span style=""><i id="time"></i></span></h4>
    </div>
    <hr>

   {{-- widget clima --}}
    <div class="col-md-12 hidden-xs col-sm-12">
        <div style="margin-left: -30px;">
          <!-- widget www.tutiempo.net - Ancho:280px - Alto:89px -->
          <div id="TT_yitgrxtxtz8BW8IUKAwjjjzDDWuULYDFrtktksi5q1z">El tiempo - Tutiempo.net</div>
          <script type="text/javascript" src="https://www.tutiempo.net/s-widget/l_yitgrxtxtz8BW8IUKAwjjjzDDWuULYDFrtktksi5q1z"></script>
        </div>

        <hr>
    </div> 
    <div class="col-xs-12 visible-xs">
      <div style="margin-left: 9px!important;">
          <!-- widget www.tutiempo.net - Ancho:280px - Alto:89px -->
          <div id="TT_yitgrxtxtz8BW8IUKAwjjjzDDWuULYDFrtktksi5q1z">El tiempo - Tutiempo.net</div>
          <script type="text/javascript" src="https://www.tutiempo.net/s-widget/l_yitgrxtxtz8BW8IUKAwjjjzDDWuULYDFrtktksi5q1z"></script>
      </div>
        <hr>
    </div>

    {{-- widget cotizacion --}}
    <p align="center">
       <iframe width="140px" height="85px" src="https://www.dolarsi.com/cotizador/cotizadorDolarsiSmall.php" frameborder="0" scrolling="0" allowfullscreen=""></iframe>
    </p>
    <hr>
    {{-- widget futbol --}}
    <p align="center">
        
        <iframe id="ftv_iframe" name="ftv_iframe" src="https://widgets.futbolenlatv.com/?color=3d85c6&culture=es-AR" width="220" height="700" frameborder="0" scrolling="auto" style="margin-bottom: 10px"></iframe>

    </p>
    
    <hr>
     <div class="col-md-12 hidden-xs hidden-sm">
        @foreach ($ads as $data)
          <a href="#" class="img-responsive your-element" data-tilt data-tilt-max="50" data-tilt-speed="300" data-tilt-perspective="950" data-tilt-glare data-tilt-max-glare="0.5">
            <img src="{{ url('imagenes_anuncios/'.$data->imagen) }}" alt="banner-ads" height="auto" width="230">
          </a><br>
        @endforeach
    </div>
     <div class="col-xs-12 col-sm-12 visible-xs visible-sm">
      @foreach ($ads as $data)
          <img  src="{{ url('imagenes_anuncios/'.$data->imagen) }}" alt="banner-ads" height="auto" width="250">
        <br>
      @endforeach
    </div>
    <div class="col-md-12 hidden-xs hidden-sm">
        <hr>
        <div class="text-center">
                <a href="{{ route('historial-noticias')}}"  class="btn btn-info" aria-label="Left Align">
                    <i class="glyphicon glyphicon-floppy-disk"></i> Historial de Noticias
                </a>
        </div>
        <hr>    
    </div>
</div>
