<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}"  xmlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    {{-- facebook share --}}
       @yield('facebook_meta')

    {{-- twitter share --}}
       @yield('twitter_meta')
     
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Noticias Taragui</title>

	@include('layout.css._css_resource')
</head>
	<body onload="startTime()">
	 @include('layout._cabecera')   
	    <header>
	      @include('layout._navbar')
	    </header>{{-- header --}}
	    @include('layout._errors')
	    @include('layout._message')
			<div class="container">
				@yield('content')
				<aside>
		            @include('layout.aside-ads')
		        </aside>
			</div><!--container-->
        </div><!--row nivel 1-->
		
		<br>
		<footer style="background-color: #424242;">
			@include('layout._footer')
		</footer>
		
		@include('layout.js._js_resource')
	</body>
</html>