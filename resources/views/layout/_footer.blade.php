		
			<hr>
				<div class="container">
				    <div class="col-xs-12" style="color: #e7e7e7;">
				    	   <div class="col-md-4 hidden-xs">
				                <ul class="list-unstyled">
				                	<i class="text-center fa fa-newspaper-o fa-5x" aria-hidden="true"><h3>Taragui Noticias</h3></i>
				                </ul>
				            </div>
				            <div style="margin-left: 50px;" class="visible-xs">
				                <ul class="list-unstyled">
				                	<i class="text-center fa fa-newspaper-o fa-5x" aria-hidden="true"><h3>Taragui Noticias</h3></i>
				                </ul>
				            </div>
				            <div class="col-md-4">
				                <ul class="list-unstyled text-center">
				                    <li><b style="font-size: 20px;">Categorias</b><li>
				                    	@foreach ($categorias as $data)
				                    	 <li>
				                    	 	<a 
				                    	 	style="color: #e7e7e7"
				                    	 	href="{{ route('path_categoria', ['name'=>$data->descripcion])}}">{{ $data->descripcion }}
				                    	 	</a>
				                    	 </li>
				                    	@endforeach
				                </ul>
				            </div>
				             <div class="col-md-4">
				                <div class="text-center center-block">
						           <b style="font-size: 20px;" class="txt-railway">Contactos</b>
						           <br>
						              {{-- <a href="https://www.facebook.com/"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
							           <a href="https://twitter.com/"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a> --}}
							           <a 
							           style="color: #e7e7e7!important"
							           href="mailto:contactos@taraguinoticias.com" data-toggle="tooltip" title="contactos@taraguinoticias.com" data-placement="bottom"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i>
							           </a>
							           <hr>
							           <i>
							           	   © {{ date('Y') }} - Taragüi Noticias.
							           </i>
							           
								</div>
				            </div>
				    </div>
				    {{-- <div class="row" style="border-top: 1px solid #f5f5f5; font-size: 13px">
				            <div class="pull-right col-xs-4">
				                <p class="text-muted pull-right">By: Fabi</p>
				            </div>
				        </div>
				     --}}
			    </div>
		