<div style="border-top: 1px solid #616161; background-color: blue;">
	@if(Auth::check())
			<button style="position: absolute;" class="btn btn-warning btn-lg" type="button" data-toggle="collapse" data-target="#collapseButtonImg-head" aria-expanded="false" aria-controls="collapseButtonImg-head">
				<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
			</button>
		<img id="img-header-new" style="width:1500px;height:300px; display: none;" src="#" alt="img_profile_new">
	@endif	
	    <img id="img-header" style="width:1500px;height:300px; display: block;" src="{{ url('imagen_cabecera/imagen_cabecera.jpg') }}" alt="img_profile">

		<div  class="profile-name animated flip visible-lg">
			<h2 class="hidden-sm">
			  Taragüi Noticias
		    </h2>
		</div>
</div>
@if(Auth::check())
	<div class="collapse" id="collapseButtonImg-head">
			<div class="well">
					<div style="background-color: grey" class="container">
							<form action="{{ route('imagen_cabecera') }}" method="POST" enctype="multipart/form-data">
								{{ csrf_field() }}
								{{ method_field('PUT') }}
							   <label for="imagen-ads">Cambiar imagen de la cabecera</label>
									<input type="hidden" name="pre-inp-img-header" value="{{ $imagen_cabecera }}">
									  <span>
										  
									  </span>
										<input type="file" accept="image/jpeg" name="inp-img-header" id="inp-img-header" onchange="showImage.call(this)"/>
									<div class="pull-right" id="btn-img-header-new">
										<input type="button" class="btn btn-default" value="cancelar" id="cancel-img-header">
										<input type="submit" class="btn btn-primary"  value="guardar">
									</div>
							</form>
					</div>
			</div>
	</div>
@endif



