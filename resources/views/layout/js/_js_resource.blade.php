	<script
			src="https://code.jquery.com/jquery-3.2.1.min.js"
			  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
			  crossorigin="anonymous"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

		<script src="{{asset('js/tilt.js')}}" type="text/javascript" charset="utf-8" async defer></script>
		<script src="{{ asset('js/mansory.min.js') }}" charset="utf-8" async defer></script>
			
		<script src="{{ asset('js/myapp.js') }}" type="text/javascript" charset="utf-8" async defer></script>
	 <script src="{{ asset('js/SocialShare.min.js') }}"></script>
	 <script type="text/javascript" charset="utf-8">
	 	$(window).bind("load resize slid.bs.carousel", function() {
	 		var imageHeight = $(".active .holder").height();
	 		$(".controllers").height( imageHeight );
	 	});
	 </script>
		
	