<nav class="clearfix navbar navbar-default navbar-static-top navbar-fixed-center">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand hidden-lg" href="#" id="profile-name-brand">
              <b>Taragüi Noticias</b>
          </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="{{ Route::currentRouteNamed('new_path') ? 'active' : ''}}"><a href="{{ route('new_path') }}">Inicio</a></li>
            @foreach ($categorias as $data)
              <li class="id{{ $data->descripcion }}">
                <a id="cat-active" href="{{ route('path_categoria', ['name'=>$data->descripcion])}}">{{$data->descripcion}}</a>
              </li>
            @endforeach   
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <div class="col-md-12">
              <form class="navbar-form" action="{{ route('path_result_new')}}"" method="GET" role="search">
                <div id="custom-search-input">
                    <div class="input-group col-md-12">
                        <input type="text" class="form-control input-sm" placeholder="Buscar" name="q" />
                        <span class="input-group-btn">
                            <button id="btn-search" class="btn btn-info btn-sm" type="button">
                                <i class="glyphicon glyphicon-search"></i>
                            </button>
                        </span>
                    </div>
                </div>
              </form>
            </div>

          </ul><!--navbar right-->
          <ul class="nav navbar-nav navbar-right">
             @if (!Auth::guest())
                  <li class="dropdown">
                   <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <i class="fa fa-user fa-fw"></i> 
                         {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard fa-fw"></i> Administracion</a>
                        </li>
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                 {{ csrf_field() }}
                            </form>
                       </li>                                    
                    </ul>
                    @endif
                    <!-- /.dropdown-user -->
                </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
   </nav>