@include('layout.css._css_resource')
<br>
<div class="container">
    <div class="jumbotron">
        <div class="text-center"><i class="fa fa-5x fa-exclamation-triangle" style="color:#d9534f;"></i></div>
        <h1 class="text-center">404 Not Found<p> </p><p><small class="text-center"> La pagina que esta buscando no existe</small></p></h1>
        <p class="text-center">Presiona el boton para volver a la pagina principal</p>
        <p class="text-center"><a class="btn btn-primary" href="{{ route('new_path') }}"><i class="fa fa-arrow-circle-left"></i> Regresar </a></p>
    </div>
</div>

