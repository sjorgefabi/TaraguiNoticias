<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Noticias Taragui</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @include('layout.css._css_resource')
    <link rel="stylesheet" type="text/css" href="{{ asset('_dashboard/css/sb-admin-2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('_dashboard/css/metisMenu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('_dashboard/css/morris.css') }}">
     <!-- Include stylesheet -->
    <link href="https://cdn.quilljs.com/1.3.2/quill.snow.css" rel="stylesheet">

    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
 
    
  
   
    
</head>
<body>
    @yield('content')
    <!-- Scripts -->
   
{{-- <script src="{{ asset('js/app.js') }}"></script> --}}
<script src="{{ asset('_dashboard/js/sb-admin-2.min.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('_dashboard/js/metisMenu.min.js') }}" type="text/javascript" charset="utf-8"></script>
    {{-- <script src="{{ asset('_dashboard/js/morris-data.js') }}" type="text/javascript" charset="utf-8" async defer></script> --}}
<script src="{{ asset('_dashboard/js/raphael.min.js') }}" type="text/javascript" charset="utf-8" async defer></script>
<script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
              integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
              crossorigin="anonymous"></script>
        <!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="{{ asset('_dashboard/js/dashboard.js') }}"> </script>


</body>
</html>
