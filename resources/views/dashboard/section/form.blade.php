@extends('dashboard._app_wrapper')
@section('body')
{{-- form para crear o editar noticias --}}
@if ($new->exists)
   <ol class="breadcrumb">
      <li><a href="{{ route('home') }}">Home</a></li>
      <li><a href="{{ route('path_noticias') }}">Noticias</a></li>
      <li>Editar Noticia</li>
    </ol>
	 <form action="{{ route('actualizarnoticia', ['new'=>$new->id])}}" method="POST" role="form" enctype="multipart/form-data">
	{{ method_field('PUT') }}
	<legend>Editar Noticia</legend>	
@else
	<ol class="breadcrumb">
      <li><a href="{{ route('home') }}">Home</a></li>
      <li><a href="{{ route('path_noticias') }}">Noticias</a></li>
      <li>Nueva Noticia</li>
    </ol>
   <form action="{{ route('guardarnoticia') }}" method="POST" role="form" enctype="multipart/form-data">
	<legend>Crear nueva noticia</legend>
@endif
    {{csrf_field()}}
		<div class="form-group">
			<input type="text" name="fecha" class="form-control" value="{{ Carbon\Carbon::parse($new->created_at)->format('Y-m-d') }}" >
			<span class="label label-default">Titulo</span>
			<input type="text" name="titulo" id="titulo" class="form-control" value="{{$new->titulo or old('titulo')}}"  title="titulo">
		</div>
		<div class="form-group">
			<span class="label label-default">Descripcion</span>
			<textarea class="summernote" name="descripcion" id="descripcion">
				{{$new->descripcion or old('descripcion')}}
			</textarea>
		</div>
		<label for="sel1">Seleccione una categoria</label>
		   <select name="categoria_id" id="categoria_id" class="form-control" required>
		   	<option value="0" disabled hidden">categorias disponibles...</option>
			@foreach ($categorias as $data)
				<option value="{{ $data->id}}" {{ $new->categoria_id == $data->id ? 'selected' : null}}>{{ $data->descripcion }}</option>
			@endforeach
		   </select>   
		   <hr>
		   <div class="checkbox">
		   	<label>
		   		<input type="checkbox" name="publicacion" {{ $new->publicacion ? 'checked' : null}}>
		   		<i data-toggle="tooltip" data-placement="right" title="Se publicara la noticia, si no esta seguro puede omitir">Publicar</i>
		   	</label>
		   	 <div class="checkbox">
		   		<label>
		   			<input type="checkbox" name="destacada" {{ $new->destacada ? 'checked' : null }}>
		   			<i data-toggle="tooltip" data-placement="right" title="Saldra en destacados">Destacar (como portada, maximo 3 por día)</i>
		   		</label>
		   	</div>
		   	{{-- <div class="checkbox">
		   		<label>
		   			<input type="checkbox" name ="portada" {{ $new->portada ? 'checked' : null }}>
		   			Portada
		   		</label>
		   	</div> --}}
		   </div>
		   <hr>
			@if ($new->imagen_destacada)
				<label>Imagen actual: </label>
				<img src="{{ url('imagenes_destacadas/'.$new->imagen_destacada) }}" alt="imagen_destacada_actual" with="100" height="100"><br>
			@endif
			<label>Imagen Destacada</label>
		    <div class="input-group image-preview">
                <input type="text" class="form-control image-preview-filename" disabled="disabled">
                <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                    <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Browse</span>
                        <input type="file" accept="image/png, image/jpeg" name="imagen_destacada" id="imagen_destacada"/>
                    </div>
                </span>
            </div>
            <hr><br>
			<div class="col-md-offset-5">
				<button type="submit" class="btn btn-success">Guardar</button>
				<a class="btn btn-primary" href="{{ route('path_noticias') }}" title="volver">Volver</a>
			</div>

</form>

@endsection
