@extends('dashboard._app_wrapper')
@section('body')
<br>
<ol class="breadcrumb">
      <li><a href="{{ route('home') }}">Home</a></li>
      <li><a href="{{ route('path_noticias') }}">Noticias</a></li>
      <li>Vista previa</li>
</ol>
<button class="btn btn-default btn-larg" data-toggle="modal" data-target="#linkNoticeModal">
      <i class="glyphicon  glyphicon-link gly-spin"></i> ->Obtener enlace de noticia
</button>
<a class="pull-right btn btn-primary" href="{{ route('path_noticias') }}" role="button">volver</a>
<hr>
  <div class="col-md-9" style="background-color: #f5f5f5">
  	<h1>{{ $new->titulo }}</h1>
	 <p>{!! $new->descripcion !!}</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		<hr>  
  </div>

 <!-- Modal -->
  <div class="modal fade" id="linkNoticeModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Enlace</h4>
        </div>
        <div class="modal-body">
          <!-- Target -->
          <p class="bg-info" id="p11">{{ url('/categoria/'.$new->categories->descripcion.'/'.$new->id)}}</p>
          <button class="btn btn-default btn-sm" onclick="copyToClipboard('#p11')">Copiar</button>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>

  <style type="text/css" media="screen">
  	.gly-spin {
  -webkit-animation: spin 2s infinite linear;
  -moz-animation: spin 2s infinite linear;
  -o-animation: spin 2s infinite linear;
  animation: spin 2s infinite linear;
}
@-moz-keyframes spin {
  0% {
    -moz-transform: rotate(0deg);
  }
  100% {
    -moz-transform: rotate(359deg);
  }
}
@-webkit-keyframes spin {
  0% {
    -webkit-transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(359deg);
  }
}
@-o-keyframes spin {
  0% {
    -o-transform: rotate(0deg);
  }
  100% {
    -o-transform: rotate(359deg);
  }
}
@keyframes spin {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(359deg);
    transform: rotate(359deg);
  }
}
  </style>
@endsection