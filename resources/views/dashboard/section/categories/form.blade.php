@extends('dashboard._app_wrapper')
@section('body')
@include('dashboard._breadcump')
@if ($data->exists)
	<form action="{{ route('actualizar_categoria', ['id'=>$data->id]) }}" method="POST" role="form">
  	 {{method_field('PUT')}}
  	<legend>Editar Categoria</legend>
@else
	<form action="{{ route('guardar_nueva_categoria') }}" method="POST" role="form">
  	<legend>Crear Categoria</legend>
@endif
{{ csrf_field() }}
  	<div class="form-group">
  		<label for="">descripcion</label>
  		<input type="text" class="form-control" id="cate_descripcion" name="descripcion" placeholder="Input field" value="{{ $data->descripcion or old('descripcion')}}">
  	</div>
    <hr>
    <div class="col-md-offset-5">
      <button type="submit" class="btn btn-primary">Guardar</button>
      <a href="{{ route('path_categorias') }}" title="volver" class="btn btn-primary">Volver</a>
    </div>
  	
  </form>
@endsection