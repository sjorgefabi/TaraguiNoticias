@extends('dashboard._app_wrapper')
@section('body')
<legend>Categorias</legend>
<input id="cantcategoria" type="hidden" value="{{ $cantcat }}">
<div class="pull-right">
	<a id="btn-crear-categoria" class="btn btn-primary" href="{{ route('crear_categoria') }}" title="">Crear categoria</a>
</div>
<br>
<table class="table table-striped table-hover">
	<thead>
		<tr>
			<th>Categoria</th>
			<th>Creado</th>
            <th>Actualizado</th>
			<th>Accion</th>
		</tr>
	</thead>
	<tbody>
	   @forelse ($categs as $cat)
			<tr>
				<td>{{ $cat->descripcion}}</td>
				<td>{{ Carbon\Carbon::parse($cat->created_at)->format('d-m-Y') }}</td>  
                <td>{{ Carbon\Carbon::parse($cat->updated_at)->format('d-m-Y') }}</td>
				<td>
					<div class="btn-group">
						<a class="btn btn-link" href="{{ route('ver_categoria', ['id'=>$cat->id]) }}" title="ver"><span class="glyphicon glyphicon-file" aria-hidden="true"></span>
						</a>
						<a class="btn btn-link" href="{{ route('editar_categoria', ['id'=>$cat->id]) }}" title="editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
						</a>
						{{-- <form action="{{ route('eliminar_categoria' , ['id'=> $cat->id]) }}" 	method="POST" role="form">
              			 	{{ csrf_field() }} {{ method_field('DELETE')}}
							<button type="submit" class="btn btn-link" title="eliminar">
								<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
							</button>
						</form> --}}
					</div>
				</td>
			</tr>
			@empty
			   <tr>
			   	<td>
			   		<div class="text-info">Sin datos</div>
			   	</td>
			   </tr>
			
		@endforelse
	</tbody>
</table>
@stop