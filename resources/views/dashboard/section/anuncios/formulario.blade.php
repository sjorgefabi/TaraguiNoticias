@extends('dashboard._app_wrapper')
@section('body')
@if ($data->exists)
  <form action="{{ route('Ads.update', ['id'=>$data->id]) }}" method="POST" role="form" enctype="multipart/form-data">
    {{ method_field('PUT') }}
    <legend>Editar Anuncio</legend>
@else
  <legend>Crear Anuncio</legend>
 <form action="{{ route('Ads.store') }}" method="POST" role="form" enctype="multipart/form-data">
@endif
{{ csrf_field() }}
  	<div class="form-group">
  		<label for="nombre">Nombre</label>
  		<input type="text" class="form-control" id="nombre" name="nombre" placeholder="nombre" value="{{ $data->nombre or old('nombre') }}">
  	</div><hr>
    @if ($data->imagen)
          <strong>Imagen actual:</strong>
          <img src="{{ url('imagenes_anuncios/'. $data->imagen) }}" alt="imagen_anuncio_actual" width="100" height="100">{{ $data->imagen }}
    @endif
    <br>
    <label for="imagen-ads">Imagen del anuncio</label>
    <input type="file" accept="image/png, image/jpeg" name="imagen-ads" id="imagen-ads"/>
    <hr>
    <div class="col-md-offset-5">
      <button type="submit" class="btn btn-primary">Guardar</button>
      <a href="{{ route('Ads.index') }}" title="volver" class="btn btn-primary">Volver</a>
    </div>
  	
  </form>
@endsection