@extends('dashboard._app_wrapper')
@section('body')
<legend>Anuncios</legend>
<div class="pull-right">
	<a id="btn-crear-categoria" class="btn btn-primary" href="{{ route('Ads.create') }}" title="">Crear Anuncio</a>
</div>
<br>
<table class="table table-striped table-hover">
	<thead>
		<tr>
			<th>Id</th>
			<th>Creado</th>
            <th>Actualizado</th>
            <th>Nombre</th>
            <th>Imagen</th>
			<th>Accion</th>
		</tr>
	</thead>
	<tbody>
		@forelse ($ads as $data)
	    <tr>
			<td>{{ $data->id }}</td>
			<td>{{ $data->created_at }}</td>
			<td>{{ $data->updated_at }}</td>
			<td>{{ $data->nombre }}</td>
			<td>{{ $data->imagen }}</td>
			<td>
			    <div class="btn-group">
						<a class="btn btn-link" href="{{ route('Ads.show', ['id' => $data->id]) }}" title="ver"><span class="glyphicon glyphicon-file" aria-hidden="true"></span>
						</a>
						<a class="btn btn-link" href="{{ route('Ads.edit', ['id' => $data->id ]) }}" title="editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
						</a>
						<form action="{{ route('Ads.destroy', ['id'=>$data->id]) }}" method="POST" role="form">
              			 	{{ csrf_field() }} {{ method_field('DELETE')}}
							<button type="submit" class="btn btn-link" title="eliminar">
								<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
							</button>
						</form>
					</div>
				</td>
			@empty
			<th> Sin datos</th>
		@endforelse
	    </tr>
	</tbody>
</table>

  
  
@stop