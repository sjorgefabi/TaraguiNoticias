@extends('dashboard._app_wrapper')
@section('body')
<div class="container-fluid">
   <div class="row">
    <ol class="breadcrumb">
      <li><a href="{{ route('home') }}">Home</a></li>
      <li>Noticias</li>
    </ol>
            <legend>Gestión de Noticias</legend>
                <div class="pull-right">
                <a href="{{ route('crearnoticia') }}" title="crear noticia" class="btn btn-primary">Agregar</a>
                </div>
        <div class="col-md-12 col-xs-12">
            <div class="table-responsive">
             <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Categoria</th>
                    <th>Creado</th>
                    <th>Actualizado</th>
                    <th>Titulo</th>
                    <th>Descripcion</th>
                    <th>publicada</th>
                    {{-- <th>destacada</th>comment --}}
                    <th>visualizacion</th>
                    <th>Accion</th>
                </tr>
            </thead>
            <tbody>
               @forelse ($news as $new)
                <tr>
                    <td>{{ $new->id }}</td>
                    <td>{{ $new->categories->descripcion or 'null' }}
                    </td>
                    <td>{{ Carbon\Carbon::parse($new->created_at)->format('d-m-Y') }}</td>  
                    <td>{{ Carbon\Carbon::parse($new->updated_at)->format('d-m-Y') }}</td>  
                    <td>{{ $new->titulo }}</td>
                    <td>{{ $new->descripcion }}</td> 
                    <td class="text-info">
                        {{ ($new->publicada) ? 'Si': 'No' }}
                    </td>
                    {{--  <td class="text-info">
                        {{ ($new->destacada) ? 'Si' : 'No' }}
                    </td>
                    --}}
                    <td>{{ $new->visualizacion }}</td>
                    <td>
                        {{-- pregunta por la id del usuario autentificado apra er los botones --}}
                        
                        <div class="btn-group">
                            <a class="btn btn-link" href="{{ route('vernoticia', ['new'=>$new]) }}" title=""><span class="glyphicon glyphicon-file" aria-hidden="true"></span></a>  
                            <a class="btn btn-link" href="{{ route('editarnoticia', ['new'=>$new->id]) }}" title="editar noticia"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                              <form action="{{ route('eliminarnoticia', ['new'=>$new->id]) }}" method="POST" role="form">
                                {{csrf_field()}}{{method_field('DELETE')}}
                                <button type="submit" class="btn btn-link"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                              </form>
                              
                        </div>
                        
                    </td>    
                </tr>  
                @empty 
                <tr> 
                 <td>
                    <div class="text-info"> Sin datos.</div>
                 </td>   
                </tr>
            @endforelse
            </tbody>
        </div>  
      </table>
    </div>{{-- table responsive --}}  
        <div class="col-md-offset-5">
            {{ $news->render() }}
        </div>
  </div>{{-- row --}}
</div>

 
@stop

