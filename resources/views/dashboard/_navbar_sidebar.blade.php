 <div class="navbar-light  sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        {{-- <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        --}}
                        <li>
                            <div class="col-md-offset-1">
                                <i class="fa fa-id-card-o fa-5x" aria-hidden="true"></i>
                            </div>
                        </li>
                        <li>
                            <a href="{{ route('home') }}"><i class="fa fa-dashboard fa-fw"></i> Inicio</a>
                        </li>
                        <li>
                            <a href="{{ route('path_noticias') }}"><i class="fa fa-newspaper-o fa-fw"></i> Noticias</a>
                        </li>
                        <li>
                            <a href="{{ route('path_categorias') }}"><i class="fa fa-edit fa-fw"></i> Categorias</a>
                        </li>
                        <li>
                            <a href="{{ route('Ads.index') }}"><i class="fa fa-edit fa-fw"></i> Anuncios</a>
                        </li>
                         {{-- <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Graficos<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#">Flot Charts</a>
                                </li>
                                <li>
                                    <a href="#l">Morris.js Charts</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        --}}
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->