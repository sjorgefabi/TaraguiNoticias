@extends('dashboard._app_wrapper')
@section('body')
                <div class="col-md-10">
                    <h1 class="page-header">Sección administrativa</h1>
                </div>
                <div class="col-md-10">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="panel panel-default">
                                    <div style="border-left: 5px solid #e53935;" class="panel-body">
                                        @if (session('status'))
                                            <div class="alert alert-success">
                                                {{ session('status') }}
                                            </div>
                                        @endif
                                        <strong>Bienvenido: </strong>{{ Auth::user()->name }}
                                            <!-- Create the editor container -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-newspaper-o fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{ $news_day }}</div>
                                    <div>Total: Noticias de hoy</div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-newspaper-o fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{ $news_week }}</div>
                                    <div>Noticias de la semana</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-newspaper-o fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{ $news_month }}</div>
                                    <div>Noticias en el mes</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-newspaper-o fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{ $news_out }}</div>
                                    <div>Total de noticias</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col-lg-4 -->
            <div class="col-md-12">
                 <!-- Trigger the modal with a button -->
              <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#CuentasModal">
                <span class="glyphicon glyphicon-th" aria-hidden="true"></span> Cuentas
              </button>

              <!-- Modal -->
              <div class="modal fade" id="CuentasModal" role="dialog">
                <div class="modal-dialog">
                
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Control de gastos</h4>
                    </div>
                    <div class="modal-body">
                      <iframe height="400" width="550" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQCp1izLajEIdJF7g9QIF1YbQ0LUWb6KwE0ThcV-UbDonZ3TL8_PDkn_UQhus89WEKMxmAfgBA8qyRL/pubhtml?gid=0&single=true"></iframe>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                  </div>
                  
                </div>
              </div>
  
            </div>
            <hr>
            <div class="col-md-offset-2">
                <div style="height: 500px!important;" id="chart_div"></div>
                    <br><br>
                    @foreach ($month_each as $data)
                     <input type="text" id="total_mes{{ $data->month }}" value="{{ $data->total }}" hidden  readonly>
                    @endforeach
            </div>
            
        </div><!-- /.row -->

        
@stop
