@extends('dashboard.sectionadmin')
@section('content')
<div id="app">
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top"  style="margin-bottom: 0">
                @include('dashboard._navbar_header')
                @include('dashboard._navbar_sidebar')
            </nav>
            <div id="page-wrapper">
            <div class="row">
                @include('layout._errors')
                @include('layout._message')

                @yield('body')
           </div><!-- /#page-wrapper -->
        </div><!-- /#wrapper -->            
    </div>
@endsection