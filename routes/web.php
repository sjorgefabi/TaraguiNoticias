<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*Route::get('/', function () {
    return view('welcome');
});*/
//administrador
/*Route::get('/admin', function(){
  return view('dashboard.sectionadmin');
});*/

Auth::routes();

Route::group(['middleware'=>'auth'], function() {
   //Dashboard
	//-gestion de noticias
	Route::get('Dashboard/Noticias', 'Dashboard\NewsAdminController@index')->name('path_noticias');
	Route::name('crearnoticia')->get('Dashboard/createNew', 'Dashboard\NewsAdminController@create');
	Route::name('guardarnoticia')->post('Dashboard/noticias', 'Dashboard\NewsAdminController@store');
	Route::name('vernoticia')->get('/Dashboard/New/{new}', 'Dashboard\NewsAdminController@show');
	Route::name('editarnoticia')->get('/Dashboard/New/{new}/edit','Dashboard\NewsAdminController@edit');
	Route::name('actualizarnoticia')->put('/Dashboard/New/{new}','Dashboard\NewsAdminController@update');
	Route::name('eliminarnoticia')->delete('Dashboard/New/{new}/delete', 'Dashboard\NewsAdminController@delete');
    //-gestion categorias
    Route::get('Dashboard/Categorias', 'Dashboard\CategoriesAdminController@index')->name('path_categorias');
    Route::get('Dashboard/Categorias/crear', 'Dashboard\CategoriesAdminController@create')->name('crear_categoria');
    Route::post('Dashboard/Categorias', 'Dashboard\CategoriesAdminController@store')->name('guardar_nueva_categoria');
    Route::get('Dashboard/Categorias/{id}', 'Dashboard\CategoriesAdminController@show')->name('ver_categoria');
    Route::get('Dashboard/Categoria/{id}/editar', 'Dashboard\CategoriesAdminController@edit')->name('editar_categoria');
    Route::put('Dashboard/Categoria/{id}', 'Dashboard\CategoriesAdminController@update')->name('actualizar_categoria');
    /*Route::delete('Dashboard/Categoria/{id}/eliminar', 'Dashboard\CategoriesAdminController@destroy')->name('eliminar_categoria');*/
    
    //gestion anuncios (ads)
    Route::resource('/Dasboard/Ads', 'Dashboard\AdsController');

});

Route::group(['middleware'=>'setLocale'], function() {

    Route::get('Taragui/Admin', 'HomeController@index')->name('home');

    /******PUBLIC*********/
    Route::get('/', 'NewsController@index')->name('new_path');
    Route::get('/resultado', 'NewsController@query')->name('path_result_new');


    /*Route::name('detalle')->get('/Noticias/{new}', 'NewsController@show');*/

    Route::put('/banner-horizontal', 'NewsController@banner')->name('new_banner_hor');
    Route::put('/banner-horizontal2', 'NewsController@banner2')->name('new_banner_hor2');


    //ruta dinamica para cada categoria 
    Route::get('/Categoria/{name}', 'NewsController@categoria')->name('path_categoria');
    //mostrar categoria
    Route::get('/Categoria/{name?}/{id}', 'NewsController@show_categoria')->name('cat_detalle');


    //imagen cabecera
    Route::put('/imagen-cabecera', 'NewsController@imgHeader')->name('imagen_cabecera');

    //historial de noticias por año y por mes del año
    Route::get('historial-noticias/{year}', 'NewsController@getNewsYear')->name('getNewsYear');
    Route::get('historial-noticias/{month}/{year}', 'NewsController@getNewsMonthYear')->name('getNewsMonthYear');
    Route::get('historial-noticias', 'NewsController@getNewsHistory')->name('historial-noticias');
});



//view composer
View::composer('*', function($view)
{
    $sharefab = 0;
    $sharetwi = 0;
    $sharewha = 0;
    $shareimg = 0;
   $view->with(['shareimg'=>$shareimg,
   'sharefab'=>$sharefab, 'sharetwi'=>$sharetwi, 'sharewha'=>$sharewha]);
});



